'use strict';

import fs         from 'fs';
import gulp       from 'gulp';
import plugins    from 'gulp-load-plugins';
import browser    from 'browser-sync';
import panini     from 'panini';
import rimraf     from 'rimraf';
import yargs      from 'yargs';
import yaml       from 'js-yaml';
import lazypipe   from 'lazypipe';
import sherpa     from 'style-sherpa';
// PostCSS processors import
// --BEM/SUIT
import bem                from 'postcss-bem';
// --syntax
import postscss           from 'postcss-scss';
// --mods
import cssAlias           from 'postcss-alias';
import cssTriangle        from 'postcss-triangle';
import cssCircle          from 'postcss-circle';
import cssCenter          from 'postcss-center';
import cssClearfix        from 'postcss-clearfix';
import cssUtils           from 'postcss-utilities';
import cssShort           from 'postcss-short';
import cssEasings         from 'postcss-easings';
// --manual grid
import cssGrid            from 'lost';
// --fallbacks
import postcssWillChange  from 'postcss-will-change';
import postcssVmin        from 'postcss-vmin';
// --optimizers
import svgo               from 'postcss-svgo';
import fontMagic          from 'postcss-font-magician';
import cssnext            from 'postcss-cssnext';
import cssPxToRem         from 'postcss-pxtorem';
import mqpacker           from 'css-mqpacker';
import sorter             from 'postcss-sorting';


// PRESETS
//--------

// Load all Gulp plugins into one variable
const $ = plugins();

// Check for "--production" flag
const PRODUCTION = !!(yargs.argv.production);

// Load settings from settings.yml
const { PORT, PATHS, POSTCSS } = loadConfig();

function loadConfig() {
  let ymlFile = fs.readFileSync('config.yml', 'utf8');
  return yaml.load(ymlFile);
}

// PostCSS processors/plugins assing
const POSTPROCESSORS = [
	bem(POSTCSS.bem), //NOTE: BEM nesting &-selectors don't work
	cssAlias,     // "@alias {cl: color; l: left; ...}"
	cssTriangle,  // "triangle: pointing-<up|down|left|right>;"
	              // "width:  [length];"
	              // "height: [length];"
	              // "background-color: [color];" (optional)
	cssCircle,    // "circle: [diameter] [color]"
	cssCenter,    // "top: center; left: center;"
	              // don't forget "position: relative" for parent
	cssClearfix,  // "clear: fix"
	cssUtils,   //GOTO: ismamz.github.io/postcss-utilities/docs
	cssShort,   //GOTO: github.com/jonathantneal/postcss-short
	cssEasings, //GOTO: easings.net
	cssGrid,    //GOTO: github.com/corysimmons/lost
	cssPxToRem(POSTCSS.pxtorem),
	postcssWillChange,
	postcssVmin,
	svgo,
	fontMagic(POSTCSS.fonts),   //NOTE: not support Cyrrilic
	cssnext(POSTCSS.autoprefix) //NOTE: CSSnext includes autoprefixer
];


// TASKS
//------

// Build the "dist" folder by running all of the below tasks
gulp.task('build',
 gulp.series(clean, gulp.parallel(pages, sass, /*jsLibs,*/ javascript, images, copy), styleGuide));

// Recombine SCSS properties in certain order
gulp.task('csssort', () =>
	gulp.src('src/assets/scss/**/*.scss')
		.pipe($.postcss([ sorter ], { syntax: postscss }))
		.pipe(gulp.dest('src/assets/scss')
));

// Just continue watching after aborting
gulp.task('watch', watch);

// Build the site, run the server, and watch for file changes
gulp.task('default',
  gulp.series('build', server, watch));


// FUNCTIONS
//----------

// Delete the "dist" folder
// This happens every time a build starts
function clean(done) {
  rimraf(PATHS.dist, done);
}

// Copy files out of the assets folder
// This task skips over the "img", "js", and "scss" folders, which are parsed separately
function copy() {
  return gulp.src( PATHS.assets )
    .pipe(gulp.dest(PATHS.dist + '/assets'));
}

// Copy page templates into finished HTML files
function pages() {
  return gulp.src('src/pages/**/*.{html,hbs,handlebars}')
    .pipe(panini( PATHS.panini ))
    .pipe(gulp.dest( PATHS.dist ));
}

// Load updated HTML templates and partials into Panini
function resetPages(done) {
  panini.refresh();
  done();
}

// Generate a style guide from the Markdown content and HTML template in styleguide/
function styleGuide(done) {
  sherpa('src/styleguide/index.md', {
    output: PATHS.dist + '/styleguide.html',
    template: 'src/styleguide/template.html'
  }, done);
}

// Compile Sass into CSS throw PostCSS
// In production, the CSS is compressed, media-queries - sorted
function sass() {
	let production = lazypipe()
		.pipe($.postcss, [ mqpacker ])
		.pipe($.cssnano);

  return gulp.src('src/assets/scss/app.scss')
    .pipe($.sourcemaps.init())
    .pipe($.sass({ includePaths: PATHS.sass.foundation })
      .on('error', $.sass.logError))
		.pipe($.postcss( POSTPROCESSORS )
			.on('error',  e => { console.error(e); watch(); })
		)
		.pipe($.if(PRODUCTION, production(), $.sourcemaps.write()))
    .pipe(gulp.dest(PATHS.dist + '/assets/css'))
    .pipe(browser.reload({ stream: true }));
}

// Make JavaScript libraries
function jsLibs() {
	return gulp.src( PATHS.jslibs )
		.pipe($.concat( 'libs.js' ))
		.pipe($.if(PRODUCTION, $.uglify()
			.on('error', e => { console.error(e); })
		))
		.pipe(gulp.dest(PATHS.dist + '/assets/js'));
}

// Combine JavaScript into one file with ES6 support
// In production, the file is minified
function javascript() {
  return gulp.src( PATHS.javascript )
    .pipe($.sourcemaps.init())
    .pipe($.concat( 'app.js' ))
    .pipe($.babel())
    .pipe($.if(PRODUCTION, $.uglify()
      .on('error', e => { console.error(e); })
    ))
    .pipe($.if(!PRODUCTION, $.sourcemaps.write()))
    .pipe(gulp.dest(PATHS.dist + '/assets/js'));
}

// Copy images to the "dist" folder
// In production, the images are compressed
function images() {
  return gulp.src('src/assets/img/**/*')
    .pipe($.if(PRODUCTION, $.imagemin({ progressive: true })))
    .pipe(gulp.dest(PATHS.dist + '/assets/img'));
}

// Start a server with BrowserSync to preview the site in
function server(done) {
  browser.init({
    server: PATHS.dist, port: PORT
  });
  done();
}

// Watch for changes to static assets, pages, Sass, and JavaScript
function watch() {
  gulp.watch(PATHS.assets, copy);
  gulp.watch('src/pages/**/*.html').on('change', gulp.series(pages, browser.reload));
	gulp.watch('src/{layouts,partials}/**/*.html').on('change', gulp.series(resetPages, pages, browser.reload));
  gulp.watch('src/assets/scss/**/*.scss', sass);
  gulp.watch('src/assets/js/**/*.js').on('change', gulp.series(javascript, browser.reload));
  gulp.watch('src/assets/img/**/*').on('change', gulp.series(images, browser.reload));
  gulp.watch('src/styleguide/**').on('change', gulp.series(styleGuide, browser.reload));
}
