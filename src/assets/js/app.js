$(document).foundation();


$('#js_mCustomScrollbar').mCustomScrollbar({
	scrollButtons: {
		enable: true
	}
});

// Fitting video size
{
	let d = document,
			w = window,
			video = d.querySelector('.js_video'),
			videoProportion = (video.videoHeight / video.videoWidth) || (9 / 16),
			logo = d.querySelector('.js_logo'),
			logoTopDist = 39;


	let videoResize = () => {
		let scrProp = w.innerHeight / w.innerWidth;

		if (videoProportion / scrProp < 1) {
			video.style.height = '100%';
			video.style.width = 'auto';
		} else {
			video.style.width = '100%';
			video.style.height = 'auto';
		}
	};
	videoResize();

	// Section titles fading on scroll
	let fading = ()=>{
		let boxes = d.querySelectorAll('.js_title-block'),
				titles = d.querySelectorAll('.js_title-block .section-title'),
				opacities = [],
				wScroll = w.scrollY || w.pageYOffset;

//		boxes.forEach( (box, i)=>{
		for (let i = boxes.length; i--;) {
			let box = boxes[i];

			if (box.offsetTop + box.clientHeight / 2 < wScroll ||
					box.offsetTop - box.clientHeight / 2 > wScroll) {
				opacities[i] = "0";

			} else {
				opacities[i] = (1 - Math.abs(box.offsetTop - wScroll) / box.clientHeight * 2).toString();
			}
		}

		requestAnimationFrame(()=>{
//			titles.forEach((title, i)=>{
			for (let i = titles.length; i--;) {
				let title = titles[i];

				title.style.opacity = opacities[i];
			};
		});
	};

	// Logo moving and fixing
	let logoTransition = ()=>{
		let wScroll = w.scrollY || w.pageYOffset,
				isFixed = wScroll < logoTopDist;

		requestAnimationFrame(()=>{
			isFixed ? logo.style.marginTop = logoTopDist - wScroll + "px" : logo.style.marginTop = "0";
		});
	};
	logoTransition();

	// Changing color of logo depending of scroll block
	let logoColorize = ()=>{
		let boxes = d.querySelectorAll('.js_light-box'),
				logoOffset = logo.clientHeight / 2,
				logoClass = "is-alt",
				wScroll = w.scrollY || w.pageYOffset;


		for (let i = boxes.length; i--;) {
			let box = boxes[i];

			if (box.offsetTop - logoOffset <= wScroll &
					box.offsetTop + box.clientHeight - logoOffset >= wScroll) {
				logo.classList.contains(logoClass) ? null : logo.classList.add(logoClass);
				return;
			}
		}
		logo.classList.contains(logoClass) ? logo.classList.remove(logoClass) : null;
	};
	logoColorize();

	// Animating calendar weeks
	let calendarStart = ()=>{
		let calendar = d.querySelector('#js_calendar'),
				wScroll = w.scrollY || w.pageYOffset;

		if (calendar.parentNode.offsetTop - w.innerHeight / 3 < wScroll) {
			calendar.classList.add('is-active');
			w.removeEventListener('scroll', calendarStart)
		}
	};

// Labels fade in
	let labelsActivate = ()=>{
		let labelsContainer = d.querySelector('.js_labels-block'),
				labelsBlock = d.querySelector('.js_labels'),
				wScroll = w.scrollY || w.pageYOffset;

		if (labelsContainer.offsetTop < wScroll) {
			labelsBlock.classList.add('is-visible');
			w.removeEventListener('scroll', labelsActivate)
		}
	};

	// Event listeners
	// -------------
	w.onresize = videoResize;
	w.onscroll = (e)=>{
		let wScroll = w.scrollY || w.pageYOffset;
		fading();
		if (wScroll < logoTopDist * 5) { logoTransition(); }
		logoColorize();
	};
	w.addEventListener('scroll', calendarStart);
	w.addEventListener('scroll', labelsActivate);
};
